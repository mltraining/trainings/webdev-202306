// --------------------------------------------
// ----- Ülesanne 1
// --------------------------------------------
// Kirjuta JavaScripti kood, mis lisaks veebilehele taustavärvi #e4e6e6
// Vihje: element.style.background

// document.querySelector()
// document.querySelectorAll()

// document.querySelector('body')
// document.title = 'rretreter'
// document.body.style.backgroundColor = '#e4e6e6'
document.querySelector('body').style.backgroundColor = '#e4e6e6'

// --------------------------------------------
// ----- Ülesanne 2
// --------------------------------------------
// Kirjuta JavaScripti kood, jooniks alla intro-klassiga elementide teksti
// Vihje: CSS=i atribuut: 'text-decoration: underline'

const introElements = document.querySelectorAll('.intro')
for (const e of introElements) {
  e.style.textDecoration = 'underline'
}

// --------------------------------------------
// ----- Ülesanne 3
// --------------------------------------------
// Kirjuta JavaScripti kood, mis selekteeriks kõik p-elemendid, mis asuvad teise article-elemendi sees.
// Käi need elemendid ükshaaval läbi ja lisa neile raam (border: 1px solid darkblue)
// ning tee nurgad ümaraks (border-radius: 3px)

const sandaalideArtikliPd = document.querySelectorAll('#article-2 p')
for (const p of sandaalideArtikliPd) {
  p.style.border = '1px solid darkblue'
  p.style.borderRadius = '3px'
}

// --------------------------------------------
// ----- Ülesanne 4
// --------------------------------------------
// Pane kolmanda article-elemendi sees oleva pealkirja teksti värviks #043e7d
const thirdArticleHeadline = document.querySelector('#article-3 h2')
thirdArticleHeadline.style.backgroundColor = '#043e7d'

// --------------------------------------------
// ----- Ülesanne 5 (keerukam)
// --------------------------------------------
// Kirjuta funktsioon, mis joonistaks ekraanile tulpdiagrammi vastavalt etteantud parameetritele.
// Näide:
// Inimeste lemmikvärvid:
// KOLLANE      ########## (20%)
// PUNANE       ###### (12%)
// SININE       ############# (26%)
// ROHELINE     ##################### (42%)
//
// Funktsioon võiks töötada nii...

// Sisendandmed...
const chartData = [
  { title: 'KOLLANE', percentageValue: 20 },
  { title: 'PUNANE', percentageValue: 12 },
  { title: 'SININE', percentageValue: 26 },
  { title: 'ROHELINE', percentageValue: 42 },
]

// Element, mille sisse kaart joonistada: <section id="chart-exercise-5"></div> (uudised.html lehel olemas)
// Diagrammi laius: 700px
// Funktsioon: drawChart(targetElementId, chartData)
