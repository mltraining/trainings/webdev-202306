let cities = [
    {
        name: 'Tallinn',
        country: 'Estonia',
        population: 434562
    },
    {
        name: 'Tartu',
        country: 'Estonia',
        population: 93865
    },
    {
        name: 'Valga',
        country: 'Estonia',
        population: 12992
    },
    {
        name: 'Võru',
        country: 'Estonia',
        population: 12367
    },
    {
        name: 'Helsinki',
        country: 'Finland',
        population: 650058
    },
    {
        name: 'Espoo',
        country: 'Finland',
        population: 284444
    },
    {
        name: 'Hanko',
        country: 'Finland',
        population: 8386
    },
    {
        name: 'Jämsä',
        country: 'Finland',
        population: 20589
    },
    {
        name: 'Stockholm',
        country: 'Sweden',
        population: 972647
    },
    {
        name: 'Uppsala',
        country: 'Sweden',
        population: 168096
    },
    {
        name: 'Lund',
        country: 'Sweden',
        population: 91940
    },
    {
        name: 'Köping',
        country: 'Sweden',
        population: 17743
    }
];

